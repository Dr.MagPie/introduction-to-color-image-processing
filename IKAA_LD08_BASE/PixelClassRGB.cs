﻿using System;

namespace IKAA_LD08_BASE
{
    public class PixelClassRGB
    {
        public byte R;
        public byte G;
        public byte B;
        public byte I;

        public PixelClassRGB(int r, int g, int b) : this(AsByte(r), AsByte(g), AsByte(b)) { }
        public PixelClassRGB(float r, float g, float b) : this(AsByte(r), AsByte(g), AsByte(b)) { }

        public PixelClassRGB(byte r = 0, byte g = 0, byte b = 0)
        {
            R = r;
            G = g;
            B = b;
            I = (byte)Math.Round(0.0722f * b + 0.715f * g + 0.212f * r);
        }


        public static PixelClassRGB operator +(PixelClassRGB a, PixelClassRGB b)
        {
            return new PixelClassRGB(AsByte(a.R + b.R), AsByte(a.G + b.G), AsByte(a.B + b.B));
        }

        public static PixelClassRGB operator -(PixelClassRGB a, PixelClassRGB b)
        {
            return new PixelClassRGB(Math.Abs(a.R - b.R), Math.Abs(a.G - b.G), Math.Abs(a.B - b.B));
        }

        public static PixelClassRGB operator *(PixelClassRGB a, PixelClassRGB b)
        {
            return new PixelClassRGB(AsByte(a.R * b.R), AsByte(a.G * b.G), AsByte(a.B * b.B));
        }
        public static PixelClassRGB operator *(int a, PixelClassRGB b)
        {
            return new PixelClassRGB(AsByte(a * b.R), AsByte(a * b.G), AsByte(a * b.B));
        }

        public static bool operator <=(PixelClassRGB a, PixelClassRGB b)
        {
            return Val(a) <= Val(b);
        }

        public static bool operator >=(PixelClassRGB a, PixelClassRGB b)
        {
            return Val(a) >= Val(b);
        }

        public static bool operator <(PixelClassRGB a, PixelClassRGB b)
        {
            return Val(a) < Val(b);
        }

        public static bool operator >(PixelClassRGB a, PixelClassRGB b)
        {
            return Val(a) > Val(b);
        }

        private static int Val(PixelClassRGB a)
        {
            return a.R + a.G + a.B;
        }

        private static byte AsByte(int value)
        {
            if (value > 255) { return 255; }
            else if (value < 0) { return 0; }
            else { return (byte)value; }
        }

        private static byte AsByte(float value)
        {
            if (value > 255) { return 255; }
            else if (value < 0) { return 0; }
            else { return (byte)value; }
        }
    }
}

