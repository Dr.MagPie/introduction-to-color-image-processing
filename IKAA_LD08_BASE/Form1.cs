﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using class imgData;

namespace IKAA_LD08_BASE
{
    public partial class Form1 : Form
    {
        public ImageClass imageClass = new ImageClass();

        public int transitionStep;

        public Form1()
        {
            InitializeComponent();
            transitionStep = 0;
        }          

        private void PictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {                
                Bitmap bmp = (Bitmap)Image.FromFile(openFileDialog1.FileName);
                imageClass.img1 = new PixelClassRGB[bmp.Width, bmp.Height];
                imageClass.ReadImage(imageClass.img1, bmp);
                pictureBox1.Image = imageClass.DrawImage(imageClass.img1);
            }
        }

        private void PictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Bitmap bmp = (Bitmap)Image.FromFile(openFileDialog1.FileName);
                imageClass.img2 = new PixelClassRGB[bmp.Width, bmp.Height];
                imageClass.ReadImage(imageClass.img2, bmp);
                pictureBox2.Image = imageClass.DrawImage(imageClass.img2);
            }
        }

        private void Blend_Button_Click(object sender, EventArgs e)
        {
            if (imageClass.img1 != null && imageClass.img2 != null)
            {
                imageClass.img3 = new PixelClassRGB[Math.Min(imageClass.img1.GetLength(0), imageClass.img2.GetLength(0)), Math.Min(imageClass.img1.GetLength(1), imageClass.img2.GetLength(1))];
                imageClass.ApplyBlendingMode(((Button)sender).Name);
                pictureBox3.Image = imageClass.DrawImage(imageClass.img3);
            }
        }

        private void Transition_Button_Click(object sender, EventArgs e)
        {

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //Timer Step number
            transitionStep++;
            if(transitionStep == 100) { timer.Stop(); }// timer.Enabled = false;
            else
            {
                //depending on step draw part of image
                pictureBox3.Image = imageClass.DrawImage(imageClass.img3);
            }
        }

        private void D_ValueChanged(object sender, EventArgs e)
        {
            imageClass.D = d.Value;
            if (imageClass.lastMode == "Opacity")
            {
                imageClass.ApplyBlendingMode("Opacity");
                pictureBox3.Image = imageClass.DrawImage(imageClass.img3);
            }
        }
    }
}
