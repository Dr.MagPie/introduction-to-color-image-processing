﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace IKAA_LD08_BASE
{
    public class ImageClass
    {
        public PixelClassRGB[,] img1;
        public PixelClassRGB[,] img2;
        public PixelClassRGB[,] img3;
        public string lastMode = "";
        private double d;
        public double D
        {
            get
            {
                return d;
            }
            set
            {
                d = value / 100.0;
            }
        }

        public void ReadImage(PixelClassRGB[,] im, Bitmap bmp)
        {
            //ieslēgt atmiņā bmp un atvērt to skenēšanai
            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);

            //pointer lai lasīt rindas
            IntPtr ptr = IntPtr.Zero;
            //cik daudz bitu ir pikselī, jeb cik daudz kanālu
            int pixelComponents; //RGB, RGBA, I
            if (bmpData.PixelFormat == PixelFormat.Format24bppRgb)
            {
                pixelComponents = 3;
            }
            else if (bmpData.PixelFormat == PixelFormat.Format32bppArgb)
            {
                pixelComponents = 4;
            }
            else
            {
                pixelComponents = 0;
            }

            // 1 attēla rinda
            var row = new byte[bmp.Width * pixelComponents];

            //skenēšana
            for (int y = 0; y < bmp.Height; y++)
            {
                //skenējam rindas (0., 1., 2., 3., ...)
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                //kopējam info no attēla rindas masīvā row
                Marshal.Copy(ptr, row, 0, row.Length);
                for (int x = 0; x < bmp.Width; x++)
                {
                    //izveidojam jaunu pixeli un ierakstam
                    //tur vērtības no rindas
                    im[x, y] = new PixelClassRGB(
                        row[pixelComponents * x + 2],
                        row[pixelComponents * x + 1],
                        row[pixelComponents * x]);
                }
            }
            bmp.UnlockBits(bmpData);
        }

        public Bitmap DrawImage(PixelClassRGB[,] img)
        {
            if (img != null)
            {
                //mainīgie
                IntPtr ptr = IntPtr.Zero;
                var bmp = new Bitmap(
                    img.GetLength(0), img.GetLength(1),
                    PixelFormat.Format24bppRgb);
                var bmpData = bmp.LockBits(
                    new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.WriteOnly, bmp.PixelFormat);
                var row = new byte[bmp.Width * 3]; //jo 24 biti!

                for (int y = 0; y < bmp.Height; y++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        row[3 * x + 2] = img[x, y].R;
                        row[3 * x + 1] = img[x, y].G;
                        row[3 * x] = img[x, y].B;
                    }
                    ptr = bmpData.Scan0 + y * bmpData.Stride;
                    Marshal.Copy(row, 0, ptr, row.Length);
                }
                bmp.UnlockBits(bmpData);
                return bmp;
            }
            else { return null; }
        }

        public void ApplyBlendingMode(string mode)
        {
            lastMode = mode;
            for (int x = 0; x < img3.GetLength(0); x++)
            {
                for (int y = 0; y < img3.GetLength(1); y++)
                {
                    switch (mode)
                    {
                        case "LinearDodge":
                            img3[x, y] = img1[x, y] + img2[x, y];
                            break;
                        case "Multiply":
                            img3[x, y] = img1[x, y] * img2[x, y];
                            break;
                        case "Difference":
                            img3[x, y] = img1[x, y] - img2[x, y];
                            break;
                        case "Lighten":
                            img3[x, y] = img2[x, y] <= img1[x, y] ? img1[x, y] : img2[x, y];
                            break;
                        case "Darken":
                            img3[x, y] = img2[x, y] <= img1[x, y] ? img2[x, y] : img1[x, y];
                            break;
                        case "Screen":
                            img3[x, y] = new PixelClassRGB(
                                (int)((1 - (1 - img2[x, y].R / 255.0) * (1 - img1[x, y].R / 255.0)) * 255),
                                (int)((1 - (1 - img2[x, y].G / 255.0) * (1 - img1[x, y].G / 255.0)) * 255),
                                (int)((1 - (1 - img2[x, y].B / 255.0) * (1 - img1[x, y].B / 255.0)) * 255));
                            break;
                        case "Subtract":
                            img3[x, y] = new PixelClassRGB(img1[x, y].R + img2[x, y].R - 255, img1[x, y].G + img2[x, y].G - 255, img1[x, y].B + img2[x, y].B - 255);
                            break;
                        case "Dodge":
                            img3[x, y] = new PixelClassRGB(
                                (int)(img2[x, y].R / 255.0 / (1 - (img1[x, y].R / 255.0)) * 255),
                                (int)(img2[x, y].G / 255.0 / (1 - (img1[x, y].G / 255.0)) * 255),
                                (int)(img2[x, y].B / 255.0 / (1 - (img1[x, y].B / 255.0)) * 255));
                            break;
                        case "Burn":
                            img3[x, y] = new PixelClassRGB(
                                (int)((1 - (1 - img2[x, y].R / 255.0) / (img1[x, y].R / 255.0)) * 255),
                                (int)((1 - (1 - img2[x, y].G / 255.0) / (img1[x, y].G / 255.0)) * 255),
                                (int)((1 - (1 - img2[x, y].B / 255.0) / (img1[x, y].B / 255.0)) * 255));
                            break;
                        case "Opacity":
                            img3[x, y] = new PixelClassRGB(
                                (int)((d * img2[x, y].R / 255.0 + (1 - d) * img1[x, y].R / 255.0) * 255),
                                (int)((d * img2[x, y].G / 255.0 + (1 - d) * img1[x, y].G / 255.0) * 255),
                                (int)((d * img2[x, y].B / 255.0 + (1 - d) * img1[x, y].B / 255.0) * 255));
                            break;
                        case "Overlay":
                            img3[x, y] = new PixelClassRGB(
                                img2[x, y].R / 255 <= 0.5 ? (int)((2 * (img1[x, y].R / 255.0) * (img2[x, y].R / 255.0)) * 255) : (int)((1 - 2 * (1 - img1[x, y].R / 255.0) * (1 - img2[x, y].R / 255.0)) * 255),
                                img2[x, y].G / 255 <= 0.5 ? (int)((2 * (img1[x, y].G / 255.0) * (img2[x, y].G / 255.0)) * 255) : (int)((1 - 2 * (1 - img1[x, y].G / 255.0) * (1 - img2[x, y].G / 255.0)) * 255),
                                img2[x, y].B / 255 <= 0.5 ? (int)((2 * (img1[x, y].B / 255.0) * (img2[x, y].B / 255.0)) * 255) : (int)((1 - 2 * (1 - img1[x, y].B / 255.0) * (1 - img2[x, y].B / 255.0)) * 255));
                            break;
                        case "HardLight":
                            img3[x, y] = new PixelClassRGB(
                                img1[x, y].R / 255 <= 0.5 ? (int)((2 * (img1[x, y].R / 255.0) * (img2[x, y].R / 255.0)) * 255) : (int)((1 - 2 * (1 - img1[x, y].R / 255.0) * (1 - img2[x, y].R / 255.0)) * 255),
                                img1[x, y].G / 255 <= 0.5 ? (int)((2 * (img1[x, y].G / 255.0) * (img2[x, y].G / 255.0)) * 255) : (int)((1 - 2 * (1 - img1[x, y].G / 255.0) * (1 - img2[x, y].G / 255.0)) * 255),
                                img1[x, y].B / 255 <= 0.5 ? (int)((2 * (img1[x, y].B / 255.0) * (img2[x, y].B / 255.0)) * 255) : (int)((1 - 2 * (1 - img1[x, y].B / 255.0) * (1 - img2[x, y].B / 255.0)) * 255));
                            break;
                        case "SoftLight":
                            // TODO
                            break;
                        case "Cool":
                            img3[x, y] = new PixelClassRGB((float)(img2[x, y].R / (255.0 - img1[x, y].R)), (float)(img2[x, y].G / (255.0 - img1[x, y].G)), (float)(img2[x, y].B / (255.0 - img1[x, y].B)));
                            break;
                    }
                }
            }
        }

        public void ApplyTransition()
        {
            for (int x = 0; x < img3.GetLength(0); x++)
            {
                for (int y = 0; y < img3.GetLength(1); y++)
                {
                    //switch case;
                    // use transition step
                }
            }
        }
    }

}
