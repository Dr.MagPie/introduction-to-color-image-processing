﻿namespace IKAA_LD08_BASE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LinearDodge = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.Wipe = new System.Windows.Forms.Button();
            this.Multiply = new System.Windows.Forms.Button();
            this.Lighten = new System.Windows.Forms.Button();
            this.Darken = new System.Windows.Forms.Button();
            this.Screen = new System.Windows.Forms.Button();
            this.Subtract = new System.Windows.Forms.Button();
            this.Dodge = new System.Windows.Forms.Button();
            this.Burn = new System.Windows.Forms.Button();
            this.Cool = new System.Windows.Forms.Button();
            this.d = new System.Windows.Forms.TrackBar();
            this.Opacity = new System.Windows.Forms.Button();
            this.Difference = new System.Windows.Forms.Button();
            this.Overlay = new System.Windows.Forms.Button();
            this.HardLight = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.d)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(288, 27);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(270, 232);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox2_MouseClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 232);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseClick);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(12, 265);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(270, 232);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Image 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(406, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Image 2";
            // 
            // LinearDodge
            // 
            this.LinearDodge.BackColor = System.Drawing.Color.Black;
            this.LinearDodge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LinearDodge.ForeColor = System.Drawing.Color.White;
            this.LinearDodge.Location = new System.Drawing.Point(288, 265);
            this.LinearDodge.Name = "LinearDodge";
            this.LinearDodge.Size = new System.Drawing.Size(77, 23);
            this.LinearDodge.TabIndex = 20;
            this.LinearDodge.Text = "LinearDodge";
            this.LinearDodge.UseVisualStyleBackColor = false;
            this.LinearDodge.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // timer
            // 
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // Wipe
            // 
            this.Wipe.BackColor = System.Drawing.Color.Black;
            this.Wipe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Wipe.ForeColor = System.Drawing.Color.White;
            this.Wipe.Location = new System.Drawing.Point(481, 265);
            this.Wipe.Name = "Wipe";
            this.Wipe.Size = new System.Drawing.Size(77, 23);
            this.Wipe.TabIndex = 21;
            this.Wipe.Text = "Wipe ->";
            this.Wipe.UseVisualStyleBackColor = false;
            this.Wipe.Click += new System.EventHandler(this.Transition_Button_Click);
            // 
            // Multiply
            // 
            this.Multiply.BackColor = System.Drawing.Color.Black;
            this.Multiply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Multiply.ForeColor = System.Drawing.Color.White;
            this.Multiply.Location = new System.Drawing.Point(288, 294);
            this.Multiply.Name = "Multiply";
            this.Multiply.Size = new System.Drawing.Size(77, 23);
            this.Multiply.TabIndex = 22;
            this.Multiply.Text = "Multiply";
            this.Multiply.UseVisualStyleBackColor = false;
            this.Multiply.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Lighten
            // 
            this.Lighten.BackColor = System.Drawing.Color.Black;
            this.Lighten.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lighten.ForeColor = System.Drawing.Color.White;
            this.Lighten.Location = new System.Drawing.Point(288, 323);
            this.Lighten.Name = "Lighten";
            this.Lighten.Size = new System.Drawing.Size(77, 23);
            this.Lighten.TabIndex = 23;
            this.Lighten.Text = "Lighten";
            this.Lighten.UseVisualStyleBackColor = false;
            this.Lighten.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Darken
            // 
            this.Darken.BackColor = System.Drawing.Color.Black;
            this.Darken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Darken.ForeColor = System.Drawing.Color.White;
            this.Darken.Location = new System.Drawing.Point(288, 352);
            this.Darken.Name = "Darken";
            this.Darken.Size = new System.Drawing.Size(77, 23);
            this.Darken.TabIndex = 24;
            this.Darken.Text = "Darken";
            this.Darken.UseVisualStyleBackColor = false;
            this.Darken.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Screen
            // 
            this.Screen.BackColor = System.Drawing.Color.Black;
            this.Screen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Screen.ForeColor = System.Drawing.Color.White;
            this.Screen.Location = new System.Drawing.Point(288, 381);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(77, 23);
            this.Screen.TabIndex = 25;
            this.Screen.Text = "Screen";
            this.Screen.UseVisualStyleBackColor = false;
            this.Screen.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Subtract
            // 
            this.Subtract.BackColor = System.Drawing.Color.Black;
            this.Subtract.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Subtract.ForeColor = System.Drawing.Color.White;
            this.Subtract.Location = new System.Drawing.Point(288, 410);
            this.Subtract.Name = "Subtract";
            this.Subtract.Size = new System.Drawing.Size(77, 23);
            this.Subtract.TabIndex = 26;
            this.Subtract.Text = "Subtract";
            this.Subtract.UseVisualStyleBackColor = false;
            this.Subtract.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Dodge
            // 
            this.Dodge.BackColor = System.Drawing.Color.Black;
            this.Dodge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Dodge.ForeColor = System.Drawing.Color.White;
            this.Dodge.Location = new System.Drawing.Point(288, 439);
            this.Dodge.Name = "Dodge";
            this.Dodge.Size = new System.Drawing.Size(77, 23);
            this.Dodge.TabIndex = 27;
            this.Dodge.Text = "Dodge";
            this.Dodge.UseVisualStyleBackColor = false;
            this.Dodge.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Burn
            // 
            this.Burn.BackColor = System.Drawing.Color.Black;
            this.Burn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Burn.ForeColor = System.Drawing.Color.White;
            this.Burn.Location = new System.Drawing.Point(288, 468);
            this.Burn.Name = "Burn";
            this.Burn.Size = new System.Drawing.Size(77, 23);
            this.Burn.TabIndex = 28;
            this.Burn.Text = "Burn";
            this.Burn.UseVisualStyleBackColor = false;
            this.Burn.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Cool
            // 
            this.Cool.BackColor = System.Drawing.Color.Black;
            this.Cool.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cool.ForeColor = System.Drawing.Color.White;
            this.Cool.Location = new System.Drawing.Point(371, 468);
            this.Cool.Name = "Cool";
            this.Cool.Size = new System.Drawing.Size(77, 23);
            this.Cool.TabIndex = 29;
            this.Cool.Text = "Cool effect";
            this.Cool.UseVisualStyleBackColor = false;
            this.Cool.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // d
            // 
            this.d.LargeChange = 10;
            this.d.Location = new System.Drawing.Point(12, 497);
            this.d.Maximum = 100;
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(270, 45);
            this.d.TabIndex = 30;
            this.d.ValueChanged += new System.EventHandler(this.D_ValueChanged);
            // 
            // Opacity
            // 
            this.Opacity.BackColor = System.Drawing.Color.Black;
            this.Opacity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Opacity.ForeColor = System.Drawing.Color.White;
            this.Opacity.Location = new System.Drawing.Point(371, 265);
            this.Opacity.Name = "Opacity";
            this.Opacity.Size = new System.Drawing.Size(77, 23);
            this.Opacity.TabIndex = 31;
            this.Opacity.Text = "Opacity";
            this.Opacity.UseVisualStyleBackColor = false;
            this.Opacity.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Difference
            // 
            this.Difference.BackColor = System.Drawing.Color.Black;
            this.Difference.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Difference.ForeColor = System.Drawing.Color.White;
            this.Difference.Location = new System.Drawing.Point(371, 294);
            this.Difference.Name = "Difference";
            this.Difference.Size = new System.Drawing.Size(77, 23);
            this.Difference.TabIndex = 32;
            this.Difference.Text = "Difference";
            this.Difference.UseVisualStyleBackColor = false;
            this.Difference.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // Overlay
            // 
            this.Overlay.BackColor = System.Drawing.Color.Black;
            this.Overlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Overlay.ForeColor = System.Drawing.Color.White;
            this.Overlay.Location = new System.Drawing.Point(371, 323);
            this.Overlay.Name = "Overlay";
            this.Overlay.Size = new System.Drawing.Size(77, 23);
            this.Overlay.TabIndex = 33;
            this.Overlay.Text = "Overlay";
            this.Overlay.UseVisualStyleBackColor = false;
            this.Overlay.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // HardLight
            // 
            this.HardLight.BackColor = System.Drawing.Color.Black;
            this.HardLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HardLight.ForeColor = System.Drawing.Color.White;
            this.HardLight.Location = new System.Drawing.Point(371, 352);
            this.HardLight.Name = "HardLight";
            this.HardLight.Size = new System.Drawing.Size(77, 23);
            this.HardLight.TabIndex = 34;
            this.HardLight.Text = "Hard Light";
            this.HardLight.UseVisualStyleBackColor = false;
            this.HardLight.Click += new System.EventHandler(this.Blend_Button_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(371, 381);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 23);
            this.button3.TabIndex = 35;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(569, 542);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.HardLight);
            this.Controls.Add(this.Overlay);
            this.Controls.Add(this.Difference);
            this.Controls.Add(this.Opacity);
            this.Controls.Add(this.d);
            this.Controls.Add(this.Cool);
            this.Controls.Add(this.Burn);
            this.Controls.Add(this.Dodge);
            this.Controls.Add(this.Subtract);
            this.Controls.Add(this.Screen);
            this.Controls.Add(this.Darken);
            this.Controls.Add(this.Lighten);
            this.Controls.Add(this.Multiply);
            this.Controls.Add(this.Wipe);
            this.Controls.Add(this.LinearDodge);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "Form1";
            this.Text = "IMAGE Transitions and Blending Modes [DS178]";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.d)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button LinearDodge;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button Wipe;
        private System.Windows.Forms.Button Multiply;
        private System.Windows.Forms.Button Lighten;
        private System.Windows.Forms.Button Darken;
        private System.Windows.Forms.Button Screen;
        private System.Windows.Forms.Button Subtract;
        private System.Windows.Forms.Button Dodge;
        private System.Windows.Forms.Button Burn;
        private System.Windows.Forms.Button Cool;
        private System.Windows.Forms.TrackBar d;
        private System.Windows.Forms.Button Opacity;
        private System.Windows.Forms.Button Difference;
        private System.Windows.Forms.Button Overlay;
        private System.Windows.Forms.Button HardLight;
        private System.Windows.Forms.Button button3;
    }
}

