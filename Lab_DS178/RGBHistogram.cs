﻿using System;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab_DS178
{
    public class RGBHistogram
    {
        public int[] R { get; set; }
        public int[] G { get; set; }
        public int[] B { get; set; }
        public int[] I { get; set; }

        public RGBHistogram()
        {
            R = new int[257];
            G = new int[257];
            B = new int[257];
            I = new int[257];
        }

        public void Clear()
        {
            for (int i = 0; i < 257; i++)
            {
                R[i] = 0;
                G[i] = 0;
                B[i] = 0;
                I[i] = 0;
            }
        }

        public void Read(RGBPixel[,] image)
        {
            Clear();
            for (int x = 0; x < image.GetLength(0); x++)
            {
                for (int y = 0; y < image.GetLength(1); y++)
                {
                    R[image[x, y].R]++;
                    G[image[x, y].G]++;
                    B[image[x, y].B]++;
                    I[image[x, y].I]++;
                }
            }
            for (int i = 0; i < 256; i++)
            {
                R[256] = Math.Max(R[256], R[i]);
                G[256] = Math.Max(G[256], G[i]);
                B[256] = Math.Max(B[256], B[i]);
                I[256] = Math.Max(I[256], I[i]);
                I[256] = Math.Max(I[256], I[i]);
            }
        }

        public void Draw(Chart chart, String mode)
        {
            chart.Series.Clear();
            chart.ChartAreas.Clear();

            chart.ChartAreas.Add("ChartArea");
            chart.ChartAreas["ChartArea"].BackColor = Color.Black;
            chart.Series.Add("R");
            chart.Series["R"].Color = Color.Red;

            chart.Series.Add("G");
            chart.Series["G"].Color = Color.Green;

            chart.Series.Add("B");
            chart.Series["B"].Color = Color.Blue;

            chart.Series.Add("I");
            chart.Series["I"].Color = Color.White;

            for (int i = 0; i < 256; i++)
            {
                chart.Series["R"].Points.AddXY(i, R[i]);
                chart.Series["G"].Points.AddXY(i, G[i]);
                chart.Series["B"].Points.AddXY(i, B[i]);
                chart.Series["I"].Points.AddXY(i, I[i]);
            }

            if (mode != "RGB")
            {
                if (mode != "R")
                {
                    chart.Series.Remove(chart.Series["R"]);
                }

                if (mode != "G")
                {
                    chart.Series.Remove(chart.Series["G"]);
                }

                if (mode != "B")
                {
                    chart.Series.Remove(chart.Series["B"]);
                }

                if (mode != "I")
                {
                    chart.Series.Remove(chart.Series["I"]);
                }
            }

        }

        private static int FindFirst(int[] histogram, int i)
        {
            if (histogram[i] != 0) { return i; }
            return FindFirst(histogram, ++i);
        }

        private static int FindLast(int[] histogram, int i)
        {
            if (histogram[i] != 0) { return i; }
            return FindLast(histogram, --i);
        }

        private static int Threshold(int threshold, int previousThreshold, int first, int last, int[] histogram)
        {
            if (threshold == previousThreshold) { return threshold; }
            previousThreshold = threshold;

            int m1 = 0, m2 = 0, p1 = 0, p2 = 0;
            for (int i = first; i < threshold; i++)
            {
                p1 += histogram[i];
                m1 += i * histogram[i];
            }
            for (int i = threshold; i < last; i++)
            {
                p2 += histogram[i];
                m2 += i * histogram[i];
            }
            if (p1 == 0) { p1 = 1; }
            if (p2 == 0) { p2 = 1; }
            m1 /= p1;
            m2 /= p2;

            threshold = (m1 + m2) / 2;
            return 0;
        }

        public int CalculateAdaptiveTreshold(int[] histogram)
        {
            int begining = FindFirst(histogram, 0);
            int end = FindLast(histogram, 256);

            int threshold = (end - begining) / 2;
            int previousThreshold = 0;
            while (threshold != previousThreshold)
            {
                previousThreshold = threshold;
                int m1 = 0, m2 = 0, p1 = 0, p2 = 0;
                for (int i = begining; i < threshold; i++)
                {
                    p1 += histogram[i];
                    m1 += i * histogram[i];
                }
                for (int i = threshold; i < end; i++)
                {
                    p2 += histogram[i];
                    m2 += i * histogram[i];
                }
                if (p1 == 0) { p1 = 1; }
                if (p2 == 0) { p2 = 1; }
                m1 /= p1;
                m2 /= p2;

                threshold = (m1 + m2) / 2;
            }
            return threshold;
        }
    }
}
