﻿using System;

namespace Lab_DS178
{
    public class YUVPixel
    {
        public byte Y { get; }
        public byte U { get; }
        public byte V { get; }

        public YUVPixel(RGBPixel p) : this(p.R, p.G, p.B) { }
        public YUVPixel(byte R = 0, byte G = 0, byte B = 0)
        {
            Y = AsByte(Convert.ToInt32(0.299 * R + 0.587 * G + 0.114 * B));
            U = AsByte(Convert.ToInt32(-0.14713 * R - 0.28886 * G + 0.436 * B + 128));
            V = AsByte(Convert.ToInt32(0.615 * R - 0.51499 * G - 0.10001 * B + 128));
        }

        public RGBPixel GetRGBPixel() => ToRGB(this);
        public RGBPixel GetChanelY() => ToRGB(this.Y, 0, 0);
        public RGBPixel GetChanelU() => ToRGB(0, this.U, 0);
        public RGBPixel GetChanelV() => ToRGB(0, 0, this.V);

        private static RGBPixel ToRGB(YUVPixel p) => ToRGB(p.Y, p.U, p.V);
        private static RGBPixel ToRGB(byte Y, byte U, byte V)
        {
            return new RGBPixel(
                AsByte(Convert.ToInt32(Y + 1.13983 * (V - 128))),
                AsByte(Convert.ToInt32(Y - 0.39465 * (U - 128) - 0.58060 * (V - 128))),
                AsByte(Convert.ToInt32(Y + 2.03211 * (U - 128))));
        }

        private static byte AsByte(int value)
        {
            if (value > 255) { return 255; }
            else if (value < 0) { return 0; }
            else { return (byte)value; }
        }

        public static implicit operator string(YUVPixel p)
        {
            return (String.Format("Y: {0} U: {1} V: {2}", p.Y, p.U, p.V));
        }
    }
}
