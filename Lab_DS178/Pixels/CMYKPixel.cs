﻿using System;

namespace Lab_DS178
{
    public class CMYKPixel
    {
        public float C { get; }
        public float M { get; }
        public float Y { get; }
        public float K { get; }

        public CMYKPixel(RGBPixel p) : this(p.R, p.G, p.B) { }
        public CMYKPixel(byte R = 0, byte G = 0, byte B = 0)
        {
            float r = R / 255f;
            float g = G / 255f;
            float b = B / 255f;

            K = ClampCmyk(1 - Math.Max(Math.Max(r, g), b));
            C = ClampCmyk((1 - r - K) / (1 - K));
            M = ClampCmyk((1 - g - K) / (1 - K));
            Y = ClampCmyk((1 - b - K) / (1 - K));
        }

        public RGBPixel GetRGBPixel() => ToRGB(this);
        public RGBPixel GetChanelC() => ToRGB(this.C, 255, 255, 255);
        public RGBPixel GetChanelM() => ToRGB(255, this.M, 255, 255);
        public RGBPixel GetChanelY() => ToRGB(255, 255, this.Y, 255);

        private static RGBPixel ToRGB(CMYKPixel p) => ToRGB(p.C, p.M, p.Y, p.K);
        private static RGBPixel ToRGB(float c, float m, float y, float k)
        {
            return new RGBPixel(
                (byte)(255 * (1 - c) * (1 - k)),
                (byte)(255 * (1 - m) * (1 - k)),
                (byte)(255 * (1 - y) * (1 - k)));
        }

        private static float ClampCmyk(float value)
        {
            if (value < 0 || float.IsNaN(value)) { value = 0; }
            return value;
        }

        public static implicit operator string(CMYKPixel p)
        {
            return (String.Format("C: {0:#.000} M: {1:#.000} Y: {2:#.000} K: {3:#.000}", p.C, p.M, p.Y, p.K));
        }
    }
}
