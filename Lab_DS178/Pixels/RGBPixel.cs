﻿using System;

namespace Lab_DS178
{
    public class RGBPixel
    {
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }
        public byte I { get; set; }

        public RGBPixel(System.Drawing.Color c) : this(c.R, c.G, c.B) { }

        public RGBPixel(byte r = 0, byte g = 0, byte b = 0)
        {
            R = r;
            G = g;
            B = b;
            if (r == 0 && g == 0 && b == 0) { I = 0; }
            else { I = (byte)Math.Round(0.0722f * b + 0.715f * g + 0.212f * r); }
        }

        public RGBPixel GetChanelR() { return new RGBPixel(this.R, 0, 0); }
        public RGBPixel GetChanelG() { return new RGBPixel(0, this.G, 0); }
        public RGBPixel GetChanelB() { return new RGBPixel(0, 0, this.G); }
        public RGBPixel GetChanelI() { return new RGBPixel(this.I, this.I, this.I); }

        public RGBPixel HSVToRGB(HSVPixel p) => HSVToRGB(p.H, p.S, p.V);
        public RGBPixel HSVToRGB(int H, byte S, byte V)
        {
            int Hi = Convert.ToInt32(H / 60);
            byte Vmin = Convert.ToByte((255 - S) * V / 255);
            int a = Convert.ToInt32((V - Vmin) * (H % 60) / 60);
            byte Vinc = Convert.ToByte(Vmin + a);
            byte Vdec = Convert.ToByte(V - a);

            switch (Hi)
            {
                case 0: return new RGBPixel(V, Vinc, Vmin);
                case 1: return new RGBPixel(Vdec, V, Vmin);
                case 2: return new RGBPixel(Vmin, V, Vinc);
                case 3: return new RGBPixel(Vmin, Vdec, V);
                case 4: return new RGBPixel(Vinc, Vmin, V);
                case 5: return new RGBPixel(V, Vmin, Vdec);
                default: return null;
            }
        }
        public RGBPixel CMYKToRGB(CMYKPixel p) => CMYKToRGB(p.C, p.M, p.Y, p.K);
        public RGBPixel CMYKToRGB(float C, float M, float Y, float K)
        {
            return new RGBPixel(
                (byte)(255 * (1 - C) * (1 - K)),
                (byte)(255 * (1 - M) * (1 - K)),
                (byte)(255 * (1 - Y) * (1 - K)));
        }
        public RGBPixel YUVToRGB(YUVPixel p) => YUVToRGB(p.Y, p.U, p.V);
        public RGBPixel YUVToRGB(double Y, double U, double V)
        {
            return new RGBPixel(
                Convert.ToByte(Y + 1.13983 * (V - 128)),
                Convert.ToByte(Y - 0.39465 * (U - 128) - 0.58060 * (V - 128)),
                Convert.ToByte(Y + 2.03211 * (U - 128)));
        }

        public static implicit operator string(RGBPixel p)
        {
            return (String.Format("R: {0} G: {1} B: {2} I: {3}", p.R, p.G, p.B, p.I));
        }
    }
}
