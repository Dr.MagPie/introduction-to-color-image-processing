﻿using System;

namespace Lab_DS178
{
    public class HSVPixel
    {
        public int H { get; }
        public byte S { get; }
        public byte V { get; }

        public HSVPixel(RGBPixel p) : this(p.R, p.G, p.B) { }
        public HSVPixel(byte R = 0, byte G = 0, byte B = 0)
        {
            int max = Math.Max(R, Math.Max(G, B));
            int min = Math.Min(R, Math.Min(G, B));
            if (max == min) { H = 0; }
            else if (max == R && G >= B) { H = 60 * (G - B) / (max - min); }
            else if (max == R && G < B) { H = 60 * (G - B) / (max - min) + 360; }
            else if (max == G) { H = 60 * (B - R) / (max - min) + 120; }
            else if (max == B) { H = 60 * (R - G) / (max - min) + 240; }
            if (H >= 300) { H = 0; }
            if (max == 0) { S = 0; }
            else { S = Convert.ToByte(255 * (1 - ((float)min / max))); }
            V = (byte)max;
        }

        public RGBPixel GetRGBPixel() => ToRGB(this);
        public RGBPixel GetChanelH() => ToRGB(this.H, 255, 255);
        public RGBPixel GetChanelS() => ToRGB(255, this.S, 255);
        public RGBPixel GetChanelV() => ToRGB(255, 255, this.V);

        private static RGBPixel ToRGB(HSVPixel p) => ToRGB(p.H, p.S, p.V);
        private static RGBPixel ToRGB(int H, byte S, byte V)
        {
            int Hi = Convert.ToInt32(H / 60);
            byte Vmin = Convert.ToByte((255 - S) * V / 255);
            int a = Convert.ToInt32((V - Vmin) * (H % 60) / 60);
            byte Vinc = Convert.ToByte(Vmin + a);
            byte Vdec = Convert.ToByte(V - a);

            switch (Hi)
            {
                case 0: return new RGBPixel(V, Vinc, Vmin);
                case 1: return new RGBPixel(Vdec, V, Vmin);
                case 2: return new RGBPixel(Vmin, V, Vinc);
                case 3: return new RGBPixel(Vmin, Vdec, V);
                case 4: return new RGBPixel(Vinc, Vmin, V);
                case 5: return new RGBPixel(V, Vmin, Vdec);
                default: return null;
            }
        }

        public static implicit operator string(HSVPixel p)
        {
            return (String.Format("H: {0} S: {1} V: {2}", p.H, p.S, p.V));
        }
    }
}
