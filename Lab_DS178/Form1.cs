﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Lab_DS178
{
    public partial class MyEditor : Form
    {
        private Color brushColor = Color.Red;
        public MyImage myImg = new MyImage();

        public MyEditor()
        {
            InitializeComponent();
            img_in.SizeMode = PictureBoxSizeMode.Zoom;
            img_out.SizeMode = PictureBoxSizeMode.Zoom;

            color_btn.BackColor = brushColor;
        }

        private void Open_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                img_in.Image = Bitmap.FromFile(dialog.FileName);
                myImg.ReadImage((Bitmap)img_in.Image.Clone());
                invert_btn.Enabled = true;
                invert_btn.Visible = true;
            }
        }

        private void Invert_btn_Click(object sender, EventArgs e)
        {
            if (img_in.Image != null)
            {
                myImg.histogram.Draw(chart1, "RGB");
                img_out.Image = myImg.GetBitmap("RGB");
                save_btn.Enabled = true;
                save_btn.Visible = true;
            }
            else
            {
                invert_btn.Enabled = false;
                invert_btn.Visible = false;
            }
        }

        private void Save_btn_Click(object sender, EventArgs e)
        {
            if (img_out.Image != null)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    img_out.Image.Save(dialog.FileName, ImageFormat.Bmp);
                }
            }
            else
            {
                save_btn.Enabled = false;
                save_btn.Visible = false;
            }
        }

        private void Color_btn_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                brushColor = dialog.Color;
                color_btn.BackColor = brushColor;
            }
        }

        private void Img_in_MouseMove(object sender, MouseEventArgs e)
        {
            if (img_in.Image != null)
            {
                if (e.Button.ToString() == "Left")
                {
                    MyPaint(e.X, e.Y, img_in);
                }
                if (e.Button.ToString() == "Right")
                {
                    ShowToolTip(e.X, e.Y, img_in);
                }
            }
        }

        private void Img_out_MouseMove(object sender, MouseEventArgs e)
        {
            if (img_out.Image != null)
            {
                if (e.Button.ToString() == "Left")
                {
                    ImgPaint(e.X, e.Y, img_out);
                }
                if (e.Button.ToString() == "Right")
                {
                    ShowToolTip(e.X, e.Y, img_out);
                }
            }
        }

        private void Chanel_CheckedChanged(object sender, EventArgs e)
        {
            if (myImg != null)
            {
                img_out.Image = myImg.GetBitmap(((RadioButton)sender).Text);
                myImg.histogram.Draw(chart1, ((RadioButton)sender).Text);
                img_out.Refresh();
            }
        }

        private void Histogram_CheckedChanged(object sender, EventArgs e)
        {
            if (myImg != null)
            {
                myImg.HistogramCutOff(((RadioButton)sender).Text);
                img_out.Image = myImg.GetBitmap(((RadioButton)sender).Text);
                myImg.histogram.Read(myImg.rgbImg);
                myImg.histogram.Draw(chart1, "RGB");
                img_out.Refresh();
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------
        private void MyPaint(int x, int y, PictureBox pb)
        {
            Point p = ConvertXY(x, y, pb);
            ((Bitmap)pb.Image).SetPixel(p.X, p.Y, brushColor);
            pb.Refresh();
        }
        private void ImgPaint(int x, int y, PictureBox pb) //DEBUG 
        {
            Point p = ConvertXY(x, y, pb);
            pb.Image = myImg.SetPixel(p.X, p.Y, brushColor);
            pb.Refresh();
        }

        private void ShowToolTip(int x, int y, PictureBox pb)
        {
            Point p = ConvertXY(x, y, pb);
            if (pb == img_in)
            {
                int R = ((Bitmap)pb.Image).GetPixel(p.X, p.Y).R;
                int G = ((Bitmap)pb.Image).GetPixel(p.X, p.Y).G;
                int B = ((Bitmap)pb.Image).GetPixel(p.X, p.Y).B;
                toolTip.SetToolTip(pb, "R: " + R + " G: " + G + " B: " + B + "\nX: " + p.X + " Y: " + p.Y);
            }
            if (pb == img_out)
            {
                toolTip.SetToolTip(pb,
                           myImg.rgbImg[p.X, p.Y] +
                    "\n" + myImg.hsvImg[p.X, p.Y] +
                    "\n" + myImg.cmykImg[p.X, p.Y] +
                    "\n" + myImg.yuvImg[p.X, p.Y] +
                    "\nX: " + p.X + " Y: " + p.Y);
            }

        }

        private Point ConvertXY(int x, int y, PictureBox pictureBox)
        {
            Point p = new Point();

            int imgWidth = pictureBox.Image.Width;
            int imgHeight = pictureBox.Image.Height;
            int boxWidth = pictureBox.Width;
            int boxHeight = pictureBox.Height;

            double kx = (double)imgWidth / boxWidth;
            double ky = (double)imgHeight / boxHeight;
            double k = Math.Max(kx, ky);

            double offsetX = (boxWidth * k - imgWidth) / 2f;
            double offsetY = (boxHeight * k - imgHeight) / 2f;

            p.X = Convert.ToInt32(Math.Max(Math.Min(Math.Round(x * k - offsetX), imgWidth - 1), 0));
            p.Y = Convert.ToInt32(Math.Max(Math.Min(Math.Round(y * k - offsetY), imgHeight - 1), 0));

            return p;
        }

        private void Noise_Click(int[,] filter)
        {
            myImg.ClearNoise(filter);
            img_out.Image = myImg.GetBitmap("RGB");
            img_out.Refresh();
        }

        private void Noise1_Click(object sender, EventArgs e)
        {
            int[,] filter = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
            Noise_Click(filter);
        }

        private void noise2_Click(object sender, EventArgs e)
        {
            int[,] filter = { { 1, 1, 1 }, { 1, 2, 1 }, { 1, 1, 1 } };
            Noise_Click(filter);
        }

        private void Noise3_Click(object sender, EventArgs e)
        {
            int[,] filter = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
            Noise_Click(filter);
        }

        private void Noise4_Click(object sender, EventArgs e)
        {
            int[,] filter = { { 0, -1, 0 }, { -1, 5, 1 }, { 0, -1, 0 } };
            Noise_Click(filter);
        }

        private void Noise5_Click(object sender, EventArgs e)
        {
            int[,] filter = { { -1, -1, 1 }, { -1, 9, -1 }, { -1, -1, -1 } };
            Noise_Click(filter);
        }

        private void Noise6_Click(object sender, EventArgs e)
        {
            int[,] filter = { { 0, -2, 0 }, { -2, 5, -2 }, { 0, -2, 0 } };
            Noise_Click(filter);
        }

        private void Segment_Click(object sender, EventArgs e)
        {
            if(img_out.Image != null)
            {
                int[,] fx_sobel = {
                    { -1, 0, 1 },
                    { -2, 0, 2 },
                    { -1, 0, 1 }
                };
                int[,] fy_sobel = {
                    { 1,   2,  1 },
                    { 0,   0,  0 },
                    { -1, -2, -1 }
                };
                myImg.EdgeSegment(fx_sobel, fy_sobel);
                img_out.Image = myImg.GetBitmap("Segment");
                myImg.histogram.Read(myImg.rgbImg2);
                myImg.histogram.Draw(chart1, "I");
                img_out.Refresh();

            }
        }

        private void Prewitt_Click(object sender, EventArgs e)
        {
            if (img_out.Image != null)
            {

                int[,] fx_prewitt = {
                { -1, 0, 1 },
                { -1, 0, 1 },
                { -1, 0, 1 }
            };
                int[,] fy_prewitt = {
                { 1,   1,  1 },
                { 0,   0,  0 },
                { -1, -1, -1 }
            };
                myImg.EdgeSegment(fx_prewitt, fy_prewitt);
                img_out.Image = myImg.GetBitmap("Segment");
                myImg.histogram.Read(myImg.rgbImg2);
                myImg.histogram.Draw(chart1, "I");
                img_out.Refresh();

            }

        }

        private void Roberts_Click(object sender, EventArgs e)
        {
            int[,] fx_roberts = {
                { -1, 0 },
                { 0, 1 }
            };
            int[,] fy_roberts = {
                { 1, 0 },
                { 0, -1 }
            };
            myImg.EdgeSegment(fx_roberts, fy_roberts);
            img_out.Image = myImg.GetBitmap("Segment");
            myImg.histogram.Read(myImg.rgbImg2);
            myImg.histogram.Draw(chart1, "I");
            img_out.Refresh();

        }

        private void Treshold_Click(object sender, EventArgs e)
        {
            if(img_out != null)
            {
                myImg.HistogramSegmentation();
                img_out.Image = myImg.GetBitmap("I");
                //Img.histogram.Read(myImg.rgbImg2);
                myImg.histogram.Draw(chart1, "I");
                img_out.Refresh();
                I.Select();
            }

        }
    }
}
