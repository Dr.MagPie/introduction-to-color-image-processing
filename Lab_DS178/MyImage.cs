﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Lab_DS178
{
    public class MyImage
    {
        private int Width { get; set; }
        private int Height { get; set; }
        public RGBPixel[,] rgbImg;
        public RGBPixel[,] rgbImg2;
        public HSVPixel[,] hsvImg;
        public CMYKPixel[,] cmykImg;
        public YUVPixel[,] yuvImg;
        public RGBHistogram histogram;

        public void ReadImage(Bitmap bmp)
        {
            Width = bmp.Width;
            Height = bmp.Height;

            rgbImg = new RGBPixel[bmp.Width, bmp.Height];
            rgbImg2 = new RGBPixel[bmp.Width, bmp.Height];
            hsvImg = new HSVPixel[bmp.Width, bmp.Height];
            cmykImg = new CMYKPixel[bmp.Width, bmp.Height];
            yuvImg = new YUVPixel[bmp.Width, bmp.Height];
            histogram = new RGBHistogram();

            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
            IntPtr ptr = IntPtr.Zero;
            int pixelComponents;

            if (bmpData.PixelFormat == PixelFormat.Format32bppArgb) { pixelComponents = 4; }
            else if (bmpData.PixelFormat == PixelFormat.Format24bppRgb) { pixelComponents = 3; }
            else { pixelComponents = 0; }

            var row = new byte[bmp.Width * pixelComponents];
            for (int y = 0; y < bmp.Height; y++)
            {
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(ptr, row, 0, row.Length);
                for (int x = 0; x < bmp.Width; x++)
                {
                    rgbImg[x, y] = new RGBPixel(row[pixelComponents * x + 2], row[pixelComponents * x + 1], row[pixelComponents * x]);
                    hsvImg[x, y] = new HSVPixel(rgbImg[x, y]);
                    cmykImg[x, y] = new CMYKPixel(rgbImg[x, y]);
                    yuvImg[x, y] = new YUVPixel(rgbImg[x, y]);
                    rgbImg2[x, y] = new RGBPixel();
                }
            }
            bmp.UnlockBits(bmpData);
            histogram.Read(rgbImg);
        }

        public void ClearNoise(int[,] filter)
        {
            int k = 0;
            //RGBPixel[,] temp = (RGBPixel[,])rgbImg.Clone();
            for (int i = 0; i < (3); i++)
            {
                for (int j = 0; j < (3); j++)
                {
                    k += filter[i, j];
                }
            }

            for (int i = 1; i < (Width - 1); i++)
            {
                for (int j = 1; j < (Height - 1); j++)
                {
                    if (i != 0 && i != Width - 1 && j != 0 && j != Height - 1)
                    {
                        int r = 0;
                        int g = 0;
                        int b = 0;

                        for (int fi = 0; fi < filter.Length; i++)
                        {
                            for (int fj = 0; fj < filter.Length; i++)
                            {
                                r += rgbImg[i + fi - 1, j + fi - 1].I * filter[fi, fj];
                                g += rgbImg[i + fi - 1, j + fi - 1].I * filter[fi, fj];
                                b += rgbImg[i + fi - 1, j + fi - 1].I * filter[fi, fj];
                            }
                        }
                        rgbImg[i, j] = new RGBPixel(AsByte(r / k), AsByte(g / k), AsByte(b / k));

                    }
                }
            }
        }

        public void EdgeSegment(int[,] fx, int[,] fy)
        {


            int[,] fx_roberts = {
                { -1, 0 },
                { 0, 1 }
            };
            int[,] fy_roberts = {
                { 1, 0 },
                { 0, -1 }
            };
            //RGBPixel[,] temp = (RGBPixel[,])rgbImg.Clone();
            if (rgbImg != null)
            {
                for (int i = 1; i < (rgbImg.GetLength(0) - 1); i++)
                {
                    for (int j = 1; j < (rgbImg.GetLength(1) - 1); j++)
                    {
                        int gx = 0;
                        int gy = 0;
                        double gs = 0;

                        for (int fi = 0; fi < fx.GetLength(0); fi++)
                        {
                            for (int fj = 0; fj < fx.GetLength(1); fj++)
                            {
                                gx += rgbImg[(i + fi) - 1, (j + fi) - 1].I * fx[fi, fj];
                                gy += rgbImg[i + fi - 1, j + fi - 1].I * fy[fi, fj];
                            }
                        }
                        gs = Math.Sqrt(Math.Pow(gx, 2) + Math.Pow(gy, 2));
                        gs = Math.Max(0, Math.Min(255, gs));
                        rgbImg2[i, j].R = (byte)gs;
                        rgbImg2[i, j].G = (byte)gs;
                        rgbImg2[i, j].B = (byte)gs;
                        rgbImg2[i, j].I = (byte)gs;
                    }
                }
            }
        }

        private static byte AsByte(int value)
        {
            if (value > 255) { return 255; }
            else if (value < 0) { return 0; }
            else { return (byte)value; }
        }

        public Bitmap GetBitmap(string mode)
        {
            if (rgbImg == null) { return null; }

            IntPtr ptr = IntPtr.Zero;
            Bitmap bmp = new Bitmap(rgbImg.GetLength(0), rgbImg.GetLength(1), PixelFormat.Format24bppRgb);
            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            var row = new byte[bmp.Width * 3];
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    RGBPixel tmp = rgbImg[x, y];
                    switch (mode)
                    {
                        case "Segment": tmp = rgbImg2[x, y]; break;
                        case "RGB": tmp = rgbImg[x, y]; break;
                        case "R": tmp = rgbImg[x, y].GetChanelR(); break;
                        case "G": tmp = rgbImg[x, y].GetChanelG(); break;
                        case "B": tmp = rgbImg[x, y].GetChanelB(); break;
                        case "I": tmp = rgbImg[x, y].GetChanelI(); break;

                        case "HSV": tmp = hsvImg[x, y].GetRGBPixel(); break;
                        case "H": tmp = hsvImg[x, y].GetChanelH(); break;
                        case "S": tmp = hsvImg[x, y].GetChanelS(); break;
                        case "V": tmp = hsvImg[x, y].GetChanelV(); break;

                        case "CMYK": tmp = cmykImg[x, y].GetRGBPixel(); break;
                        case "C": tmp = cmykImg[x, y].GetChanelC(); break;
                        case "M": tmp = cmykImg[x, y].GetChanelM(); break;
                        case "Y": tmp = cmykImg[x, y].GetChanelY(); break;

                        case "YUV": tmp = yuvImg[x, y].GetRGBPixel(); break;
                        case "Yu": tmp = yuvImg[x, y].GetChanelY(); break;
                        case "U": tmp = yuvImg[x, y].GetChanelU(); break;
                        case "uV": tmp = yuvImg[x, y].GetChanelV(); break;
                    }

                    row[3 * x + 2] = tmp.R;
                    row[3 * x + 1] = tmp.G;
                    row[3 * x] = tmp.B;
                }
                ptr = bmpData.Scan0 + y * bmpData.Stride;
                Marshal.Copy(row, 0, ptr, row.Length);
            }

            bmp.UnlockBits(bmpData);

            return bmp;
        }

        public Bitmap SetPixel(int x, int y, Color color)
        {
            if (x < Width && y < Height)
            {
                RGBPixel tmp = new RGBPixel(color);
                rgbImg[x, y] = tmp;
                hsvImg[x, y] = new HSVPixel(tmp);
                cmykImg[x, y] = new CMYKPixel(tmp);
                yuvImg[x, y] = new YUVPixel(tmp);
            }
            return GetBitmap("RGB");
        }

        public void HistogramSegmentation()
        {
            if(rgbImg != null)
            {
                int T = histogram.CalculateAdaptiveTreshold(histogram.I);
                for (int x = 0; x < rgbImg.GetLength(0); x++)
                {
                    for (int y = 0; y < rgbImg.GetLength(1); y++)
                    {
                        if (rgbImg[x, y].I <= T) { rgbImg[x, y].I = 0; }
                        else { rgbImg[x, y].I = 255; }
                    }
                }
            }
        }

        public void HistogramCutOff(string mode)
        {
            int desired = 255;
            int begining = 0;
            int end = 0;
            switch (mode)
            {
                case "R":
                    begining = CalculateBegining(begining, histogram.R, 0);
                    end = CalculateEnd(end, histogram.R, 256);
                    break;
                case "G":
                    begining = CalculateBegining(begining, histogram.G, 0);
                    end = CalculateEnd(end, histogram.G, 256);
                    break;
                case "B":
                    begining = CalculateBegining(begining, histogram.B, 0);
                    end = CalculateEnd(end, histogram.B, 256);
                    break;
                case "I":
                    begining = CalculateBegining(begining, histogram.I, 0);
                    end = CalculateEnd(end, histogram.I, 256);
                    break;
            }


            int original = end - begining;
            float k = desired / (float)original;

            for (int x = 0; x < rgbImg.GetLength(0); x++)
            {
                for (int y = 0; y < rgbImg.GetLength(1); y++)
                {
                    switch (mode)
                    {
                        case "R": rgbImg[x, y].R = AsByte(k * (rgbImg[x, y].R - begining)); break;
                        case "G": rgbImg[x, y].G = AsByte(k * (rgbImg[x, y].G - begining)); break;
                        case "B": rgbImg[x, y].B = AsByte(k * (rgbImg[x, y].B - begining)); break;
                        case "I": rgbImg[x, y].I = AsByte(k * (rgbImg[x, y].I - begining)); break;
                    }
                }
            }
        }

        private static int CalculateBegining(int begining, int[] histogram, int i)
        {
            if (begining != 0) { return i; }
            return CalculateBegining(histogram[i], histogram, ++i);
        }

        private static int CalculateEnd(int end, int[] histogram, int i)
        {
            if (end != 0) { return i; }
            return CalculateEnd(histogram[i], histogram, --i);
        }

        private static byte AsByte(float value)
        {
            if (value > 255) { return 255; }
            else if (value < 0) { return 0; }
            else { return (byte)value; }
        }
    }
}
