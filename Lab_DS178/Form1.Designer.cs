﻿namespace Lab_DS178
{
    partial class MyEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyEditor));
            this.img_in = new System.Windows.Forms.PictureBox();
            this.img_out = new System.Windows.Forms.PictureBox();
            this.open_btn = new System.Windows.Forms.Button();
            this.invert_btn = new System.Windows.Forms.Button();
            this.save_btn = new System.Windows.Forms.Button();
            this.color_btn = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.RGB = new System.Windows.Forms.RadioButton();
            this.HSV = new System.Windows.Forms.RadioButton();
            this.R = new System.Windows.Forms.RadioButton();
            this.G = new System.Windows.Forms.RadioButton();
            this.B = new System.Windows.Forms.RadioButton();
            this.H = new System.Windows.Forms.RadioButton();
            this.V = new System.Windows.Forms.RadioButton();
            this.S = new System.Windows.Forms.RadioButton();
            this.chanel_groupBox = new System.Windows.Forms.GroupBox();
            this.I = new System.Windows.Forms.RadioButton();
            this.uV = new System.Windows.Forms.RadioButton();
            this.U = new System.Windows.Forms.RadioButton();
            this.Yu = new System.Windows.Forms.RadioButton();
            this.YUV = new System.Windows.Forms.RadioButton();
            this.CMYK = new System.Windows.Forms.RadioButton();
            this.Y = new System.Windows.Forms.RadioButton();
            this.M = new System.Windows.Forms.RadioButton();
            this.C = new System.Windows.Forms.RadioButton();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.histogram = new System.Windows.Forms.GroupBox();
            this.bCutoff = new System.Windows.Forms.RadioButton();
            this.gCutoff = new System.Windows.Forms.RadioButton();
            this.rCuttof = new System.Windows.Forms.RadioButton();
            this.noise1 = new System.Windows.Forms.Button();
            this.noise2 = new System.Windows.Forms.Button();
            this.noise3 = new System.Windows.Forms.Button();
            this.noise4 = new System.Windows.Forms.Button();
            this.noise5 = new System.Windows.Forms.Button();
            this.noise6 = new System.Windows.Forms.Button();
            this.Segment = new System.Windows.Forms.Button();
            this.Roberts = new System.Windows.Forms.Button();
            this.Prewitt = new System.Windows.Forms.Button();
            this.treshold = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.img_in)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_out)).BeginInit();
            this.chanel_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.histogram.SuspendLayout();
            this.SuspendLayout();
            // 
            // img_in
            // 
            this.img_in.Cursor = System.Windows.Forms.Cursors.Default;
            this.img_in.Location = new System.Drawing.Point(12, 12);
            this.img_in.Name = "img_in";
            this.img_in.Size = new System.Drawing.Size(500, 500);
            this.img_in.TabIndex = 0;
            this.img_in.TabStop = false;
            this.img_in.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Img_in_MouseMove);
            this.img_in.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Img_in_MouseMove);
            // 
            // img_out
            // 
            this.img_out.Location = new System.Drawing.Point(518, 12);
            this.img_out.Name = "img_out";
            this.img_out.Size = new System.Drawing.Size(500, 500);
            this.img_out.TabIndex = 1;
            this.img_out.TabStop = false;
            this.img_out.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Img_out_MouseMove);
            this.img_out.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Img_out_MouseMove);
            // 
            // open_btn
            // 
            this.open_btn.BackColor = System.Drawing.SystemColors.Desktop;
            this.open_btn.FlatAppearance.BorderSize = 0;
            this.open_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.open_btn.ForeColor = System.Drawing.SystemColors.Window;
            this.open_btn.Location = new System.Drawing.Point(1083, 12);
            this.open_btn.Name = "open_btn";
            this.open_btn.Size = new System.Drawing.Size(53, 31);
            this.open_btn.TabIndex = 3;
            this.open_btn.Text = "Open";
            this.open_btn.UseVisualStyleBackColor = false;
            this.open_btn.Click += new System.EventHandler(this.Open_btn_Click);
            // 
            // invert_btn
            // 
            this.invert_btn.BackColor = System.Drawing.SystemColors.Desktop;
            this.invert_btn.Enabled = false;
            this.invert_btn.FlatAppearance.BorderSize = 0;
            this.invert_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.invert_btn.ForeColor = System.Drawing.SystemColors.Window;
            this.invert_btn.Location = new System.Drawing.Point(1024, 49);
            this.invert_btn.Name = "invert_btn";
            this.invert_btn.Size = new System.Drawing.Size(53, 31);
            this.invert_btn.TabIndex = 4;
            this.invert_btn.Text = "Invert";
            this.invert_btn.UseVisualStyleBackColor = false;
            this.invert_btn.Visible = false;
            this.invert_btn.Click += new System.EventHandler(this.Invert_btn_Click);
            // 
            // save_btn
            // 
            this.save_btn.BackColor = System.Drawing.SystemColors.Desktop;
            this.save_btn.FlatAppearance.BorderSize = 0;
            this.save_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save_btn.ForeColor = System.Drawing.SystemColors.Window;
            this.save_btn.Location = new System.Drawing.Point(1083, 49);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(53, 31);
            this.save_btn.TabIndex = 5;
            this.save_btn.Text = "Save";
            this.save_btn.UseVisualStyleBackColor = false;
            this.save_btn.Click += new System.EventHandler(this.Save_btn_Click);
            // 
            // color_btn
            // 
            this.color_btn.BackColor = System.Drawing.SystemColors.Desktop;
            this.color_btn.FlatAppearance.BorderSize = 0;
            this.color_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.color_btn.ForeColor = System.Drawing.SystemColors.Window;
            this.color_btn.Location = new System.Drawing.Point(1024, 12);
            this.color_btn.Name = "color_btn";
            this.color_btn.Size = new System.Drawing.Size(53, 31);
            this.color_btn.TabIndex = 6;
            this.color_btn.UseVisualStyleBackColor = false;
            this.color_btn.Click += new System.EventHandler(this.Color_btn_Click);
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.BackColor = System.Drawing.SystemColors.InfoText;
            this.toolTip.ForeColor = System.Drawing.SystemColors.Info;
            this.toolTip.InitialDelay = 1;
            this.toolTip.IsBalloon = true;
            this.toolTip.ReshowDelay = 100;
            this.toolTip.UseFading = false;
            // 
            // RGB
            // 
            this.RGB.AutoSize = true;
            this.RGB.Checked = true;
            this.RGB.Location = new System.Drawing.Point(6, 19);
            this.RGB.Name = "RGB";
            this.RGB.Size = new System.Drawing.Size(48, 17);
            this.RGB.TabIndex = 7;
            this.RGB.TabStop = true;
            this.RGB.Text = "RGB";
            this.RGB.UseVisualStyleBackColor = true;
            this.RGB.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // HSV
            // 
            this.HSV.AutoSize = true;
            this.HSV.Location = new System.Drawing.Point(59, 19);
            this.HSV.Name = "HSV";
            this.HSV.Size = new System.Drawing.Size(47, 17);
            this.HSV.TabIndex = 8;
            this.HSV.Text = "HSV";
            this.HSV.UseVisualStyleBackColor = true;
            this.HSV.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // R
            // 
            this.R.AutoSize = true;
            this.R.ForeColor = System.Drawing.Color.Red;
            this.R.Location = new System.Drawing.Point(6, 61);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(33, 17);
            this.R.TabIndex = 9;
            this.R.Text = "R";
            this.R.UseVisualStyleBackColor = true;
            this.R.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // G
            // 
            this.G.AutoSize = true;
            this.G.ForeColor = System.Drawing.Color.Green;
            this.G.Location = new System.Drawing.Point(44, 61);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(33, 17);
            this.G.TabIndex = 10;
            this.G.Text = "G";
            this.G.UseVisualStyleBackColor = true;
            this.G.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.ForeColor = System.Drawing.Color.MediumBlue;
            this.B.Location = new System.Drawing.Point(83, 61);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(32, 17);
            this.B.TabIndex = 11;
            this.B.Text = "B";
            this.B.UseVisualStyleBackColor = true;
            this.B.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // H
            // 
            this.H.AutoSize = true;
            this.H.Location = new System.Drawing.Point(6, 84);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(33, 17);
            this.H.TabIndex = 12;
            this.H.Text = "H";
            this.H.UseVisualStyleBackColor = true;
            this.H.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // V
            // 
            this.V.AutoSize = true;
            this.V.Location = new System.Drawing.Point(83, 84);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(32, 17);
            this.V.TabIndex = 13;
            this.V.Text = "V";
            this.V.UseVisualStyleBackColor = true;
            this.V.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // S
            // 
            this.S.AutoSize = true;
            this.S.Location = new System.Drawing.Point(45, 84);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(32, 17);
            this.S.TabIndex = 14;
            this.S.Text = "S";
            this.S.UseVisualStyleBackColor = true;
            this.S.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // chanel_groupBox
            // 
            this.chanel_groupBox.Controls.Add(this.I);
            this.chanel_groupBox.Controls.Add(this.uV);
            this.chanel_groupBox.Controls.Add(this.U);
            this.chanel_groupBox.Controls.Add(this.Yu);
            this.chanel_groupBox.Controls.Add(this.YUV);
            this.chanel_groupBox.Controls.Add(this.CMYK);
            this.chanel_groupBox.Controls.Add(this.RGB);
            this.chanel_groupBox.Controls.Add(this.S);
            this.chanel_groupBox.Controls.Add(this.Y);
            this.chanel_groupBox.Controls.Add(this.HSV);
            this.chanel_groupBox.Controls.Add(this.V);
            this.chanel_groupBox.Controls.Add(this.M);
            this.chanel_groupBox.Controls.Add(this.R);
            this.chanel_groupBox.Controls.Add(this.H);
            this.chanel_groupBox.Controls.Add(this.C);
            this.chanel_groupBox.Controls.Add(this.G);
            this.chanel_groupBox.Controls.Add(this.B);
            this.chanel_groupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chanel_groupBox.ForeColor = System.Drawing.SystemColors.Menu;
            this.chanel_groupBox.Location = new System.Drawing.Point(1024, 86);
            this.chanel_groupBox.Name = "chanel_groupBox";
            this.chanel_groupBox.Size = new System.Drawing.Size(122, 174);
            this.chanel_groupBox.TabIndex = 15;
            this.chanel_groupBox.TabStop = false;
            this.chanel_groupBox.Text = "Chanels";
            // 
            // I
            // 
            this.I.AutoSize = true;
            this.I.ForeColor = System.Drawing.Color.Gray;
            this.I.Location = new System.Drawing.Point(7, 150);
            this.I.Name = "I";
            this.I.Size = new System.Drawing.Size(28, 17);
            this.I.TabIndex = 23;
            this.I.Text = "I";
            this.I.UseVisualStyleBackColor = true;
            this.I.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // uV
            // 
            this.uV.AutoSize = true;
            this.uV.Location = new System.Drawing.Point(83, 127);
            this.uV.Name = "uV";
            this.uV.Size = new System.Drawing.Size(38, 17);
            this.uV.TabIndex = 22;
            this.uV.Text = "uV";
            this.uV.UseVisualStyleBackColor = true;
            this.uV.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // U
            // 
            this.U.AutoSize = true;
            this.U.Location = new System.Drawing.Point(45, 127);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(33, 17);
            this.U.TabIndex = 21;
            this.U.Text = "U";
            this.U.UseVisualStyleBackColor = true;
            this.U.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // Yu
            // 
            this.Yu.AutoSize = true;
            this.Yu.Location = new System.Drawing.Point(7, 127);
            this.Yu.Name = "Yu";
            this.Yu.Size = new System.Drawing.Size(38, 17);
            this.Yu.TabIndex = 20;
            this.Yu.Text = "Yu";
            this.Yu.UseVisualStyleBackColor = true;
            this.Yu.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // YUV
            // 
            this.YUV.AutoSize = true;
            this.YUV.Location = new System.Drawing.Point(59, 38);
            this.YUV.Name = "YUV";
            this.YUV.Size = new System.Drawing.Size(47, 17);
            this.YUV.TabIndex = 19;
            this.YUV.Text = "YUV";
            this.YUV.UseVisualStyleBackColor = true;
            this.YUV.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // CMYK
            // 
            this.CMYK.AutoSize = true;
            this.CMYK.Location = new System.Drawing.Point(6, 38);
            this.CMYK.Name = "CMYK";
            this.CMYK.Size = new System.Drawing.Size(55, 17);
            this.CMYK.TabIndex = 18;
            this.CMYK.Text = "CMYK";
            this.CMYK.UseVisualStyleBackColor = true;
            this.CMYK.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // Y
            // 
            this.Y.AutoSize = true;
            this.Y.ForeColor = System.Drawing.Color.Yellow;
            this.Y.Location = new System.Drawing.Point(83, 107);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(32, 17);
            this.Y.TabIndex = 16;
            this.Y.Text = "Y";
            this.Y.UseVisualStyleBackColor = true;
            this.Y.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // M
            // 
            this.M.AutoSize = true;
            this.M.ForeColor = System.Drawing.Color.Magenta;
            this.M.Location = new System.Drawing.Point(45, 107);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(34, 17);
            this.M.TabIndex = 16;
            this.M.Text = "M";
            this.M.UseVisualStyleBackColor = true;
            this.M.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.ForeColor = System.Drawing.Color.Cyan;
            this.C.Location = new System.Drawing.Point(6, 107);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(32, 17);
            this.C.TabIndex = 15;
            this.C.Text = "C";
            this.C.UseVisualStyleBackColor = true;
            this.C.CheckedChanged += new System.EventHandler(this.Chanel_CheckedChanged);
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            this.chart1.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.BackSecondaryColor = System.Drawing.Color.Transparent;
            chartArea1.BorderColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            chartArea1.ShadowColor = System.Drawing.Color.Transparent;
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chart1.Location = new System.Drawing.Point(518, 518);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(500, 275);
            this.chart1.TabIndex = 16;
            this.chart1.Text = "chart1";
            // 
            // histogram
            // 
            this.histogram.Controls.Add(this.bCutoff);
            this.histogram.Controls.Add(this.gCutoff);
            this.histogram.Controls.Add(this.rCuttof);
            this.histogram.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.histogram.ForeColor = System.Drawing.SystemColors.Menu;
            this.histogram.Location = new System.Drawing.Point(1024, 306);
            this.histogram.Name = "histogram";
            this.histogram.Size = new System.Drawing.Size(122, 44);
            this.histogram.TabIndex = 23;
            this.histogram.TabStop = false;
            this.histogram.Text = "Histogram";
            // 
            // bCutoff
            // 
            this.bCutoff.AutoSize = true;
            this.bCutoff.ForeColor = System.Drawing.Color.MediumBlue;
            this.bCutoff.Location = new System.Drawing.Point(81, 19);
            this.bCutoff.Name = "bCutoff";
            this.bCutoff.Size = new System.Drawing.Size(32, 17);
            this.bCutoff.TabIndex = 23;
            this.bCutoff.Text = "B";
            this.bCutoff.UseVisualStyleBackColor = true;
            this.bCutoff.CheckedChanged += new System.EventHandler(this.Histogram_CheckedChanged);
            // 
            // gCutoff
            // 
            this.gCutoff.AutoSize = true;
            this.gCutoff.ForeColor = System.Drawing.Color.Green;
            this.gCutoff.Location = new System.Drawing.Point(45, 19);
            this.gCutoff.Name = "gCutoff";
            this.gCutoff.Size = new System.Drawing.Size(33, 17);
            this.gCutoff.TabIndex = 24;
            this.gCutoff.Text = "G";
            this.gCutoff.UseVisualStyleBackColor = true;
            this.gCutoff.CheckedChanged += new System.EventHandler(this.Histogram_CheckedChanged);
            // 
            // rCuttof
            // 
            this.rCuttof.AutoSize = true;
            this.rCuttof.ForeColor = System.Drawing.Color.Red;
            this.rCuttof.Location = new System.Drawing.Point(6, 19);
            this.rCuttof.Name = "rCuttof";
            this.rCuttof.Size = new System.Drawing.Size(33, 17);
            this.rCuttof.TabIndex = 23;
            this.rCuttof.Text = "R";
            this.rCuttof.UseVisualStyleBackColor = true;
            this.rCuttof.CheckedChanged += new System.EventHandler(this.Histogram_CheckedChanged);
            // 
            // noise1
            // 
            this.noise1.BackColor = System.Drawing.SystemColors.Desktop;
            this.noise1.FlatAppearance.BorderSize = 0;
            this.noise1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noise1.ForeColor = System.Drawing.SystemColors.Window;
            this.noise1.Location = new System.Drawing.Point(1024, 356);
            this.noise1.Name = "noise1";
            this.noise1.Size = new System.Drawing.Size(53, 31);
            this.noise1.TabIndex = 24;
            this.noise1.Text = "Noise1";
            this.noise1.UseVisualStyleBackColor = false;
            this.noise1.Click += new System.EventHandler(this.Noise1_Click);
            // 
            // noise2
            // 
            this.noise2.BackColor = System.Drawing.SystemColors.Desktop;
            this.noise2.FlatAppearance.BorderSize = 0;
            this.noise2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noise2.ForeColor = System.Drawing.SystemColors.Window;
            this.noise2.Location = new System.Drawing.Point(1093, 356);
            this.noise2.Name = "noise2";
            this.noise2.Size = new System.Drawing.Size(53, 31);
            this.noise2.TabIndex = 25;
            this.noise2.Text = "Noise2";
            this.noise2.UseVisualStyleBackColor = false;
            this.noise2.Click += new System.EventHandler(this.noise2_Click);
            // 
            // noise3
            // 
            this.noise3.BackColor = System.Drawing.SystemColors.Desktop;
            this.noise3.FlatAppearance.BorderSize = 0;
            this.noise3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noise3.ForeColor = System.Drawing.SystemColors.Window;
            this.noise3.Location = new System.Drawing.Point(1026, 393);
            this.noise3.Name = "noise3";
            this.noise3.Size = new System.Drawing.Size(53, 31);
            this.noise3.TabIndex = 26;
            this.noise3.Text = "Noise3";
            this.noise3.UseVisualStyleBackColor = false;
            this.noise3.Click += new System.EventHandler(this.Noise3_Click);
            // 
            // noise4
            // 
            this.noise4.BackColor = System.Drawing.SystemColors.Desktop;
            this.noise4.FlatAppearance.BorderSize = 0;
            this.noise4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noise4.ForeColor = System.Drawing.SystemColors.Window;
            this.noise4.Location = new System.Drawing.Point(1093, 393);
            this.noise4.Name = "noise4";
            this.noise4.Size = new System.Drawing.Size(53, 31);
            this.noise4.TabIndex = 27;
            this.noise4.Text = "Noise4";
            this.noise4.UseVisualStyleBackColor = false;
            this.noise4.Click += new System.EventHandler(this.Noise4_Click);
            // 
            // noise5
            // 
            this.noise5.BackColor = System.Drawing.SystemColors.Desktop;
            this.noise5.FlatAppearance.BorderSize = 0;
            this.noise5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noise5.ForeColor = System.Drawing.SystemColors.Window;
            this.noise5.Location = new System.Drawing.Point(1026, 430);
            this.noise5.Name = "noise5";
            this.noise5.Size = new System.Drawing.Size(53, 31);
            this.noise5.TabIndex = 28;
            this.noise5.Text = "Noise5";
            this.noise5.UseVisualStyleBackColor = false;
            this.noise5.Click += new System.EventHandler(this.Noise5_Click);
            // 
            // noise6
            // 
            this.noise6.BackColor = System.Drawing.SystemColors.Desktop;
            this.noise6.FlatAppearance.BorderSize = 0;
            this.noise6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noise6.ForeColor = System.Drawing.SystemColors.Window;
            this.noise6.Location = new System.Drawing.Point(1094, 430);
            this.noise6.Name = "noise6";
            this.noise6.Size = new System.Drawing.Size(53, 31);
            this.noise6.TabIndex = 29;
            this.noise6.Text = "Noise6";
            this.noise6.UseVisualStyleBackColor = false;
            this.noise6.Click += new System.EventHandler(this.Noise6_Click);
            // 
            // Segment
            // 
            this.Segment.BackColor = System.Drawing.SystemColors.Desktop;
            this.Segment.FlatAppearance.BorderSize = 0;
            this.Segment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Segment.ForeColor = System.Drawing.SystemColors.Window;
            this.Segment.Location = new System.Drawing.Point(1024, 467);
            this.Segment.Name = "Segment";
            this.Segment.Size = new System.Drawing.Size(76, 31);
            this.Segment.TabIndex = 30;
            this.Segment.Text = "Sobel";
            this.Segment.UseVisualStyleBackColor = false;
            this.Segment.Click += new System.EventHandler(this.Segment_Click);
            // 
            // Roberts
            // 
            this.Roberts.BackColor = System.Drawing.SystemColors.Desktop;
            this.Roberts.FlatAppearance.BorderSize = 0;
            this.Roberts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Roberts.ForeColor = System.Drawing.SystemColors.Window;
            this.Roberts.Location = new System.Drawing.Point(1024, 504);
            this.Roberts.Name = "Roberts";
            this.Roberts.Size = new System.Drawing.Size(76, 31);
            this.Roberts.TabIndex = 31;
            this.Roberts.Text = "Roberts";
            this.Roberts.UseVisualStyleBackColor = false;
            this.Roberts.Click += new System.EventHandler(this.Roberts_Click);
            // 
            // Prewitt
            // 
            this.Prewitt.BackColor = System.Drawing.SystemColors.Desktop;
            this.Prewitt.FlatAppearance.BorderSize = 0;
            this.Prewitt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Prewitt.ForeColor = System.Drawing.SystemColors.Window;
            this.Prewitt.Location = new System.Drawing.Point(1024, 541);
            this.Prewitt.Name = "Prewitt";
            this.Prewitt.Size = new System.Drawing.Size(76, 31);
            this.Prewitt.TabIndex = 32;
            this.Prewitt.Text = "Prewitt";
            this.Prewitt.UseVisualStyleBackColor = false;
            this.Prewitt.Click += new System.EventHandler(this.Prewitt_Click);
            // 
            // treshold
            // 
            this.treshold.BackColor = System.Drawing.SystemColors.Desktop;
            this.treshold.FlatAppearance.BorderSize = 0;
            this.treshold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.treshold.ForeColor = System.Drawing.SystemColors.Window;
            this.treshold.Location = new System.Drawing.Point(1026, 578);
            this.treshold.Name = "treshold";
            this.treshold.Size = new System.Drawing.Size(76, 31);
            this.treshold.TabIndex = 33;
            this.treshold.Text = "Treshold";
            this.treshold.UseVisualStyleBackColor = false;
            this.treshold.Click += new System.EventHandler(this.Treshold_Click);
            // 
            // MyEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1148, 805);
            this.Controls.Add(this.treshold);
            this.Controls.Add(this.Prewitt);
            this.Controls.Add(this.Roberts);
            this.Controls.Add(this.Segment);
            this.Controls.Add(this.noise6);
            this.Controls.Add(this.noise5);
            this.Controls.Add(this.noise4);
            this.Controls.Add(this.noise3);
            this.Controls.Add(this.noise2);
            this.Controls.Add(this.noise1);
            this.Controls.Add(this.histogram);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.chanel_groupBox);
            this.Controls.Add(this.color_btn);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.invert_btn);
            this.Controls.Add(this.open_btn);
            this.Controls.Add(this.img_out);
            this.Controls.Add(this.img_in);
            this.ForeColor = System.Drawing.SystemColors.Window;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(2000, 1000);
            this.MinimumSize = new System.Drawing.Size(1164, 562);
            this.Name = "MyEditor";
            this.Text = "DS178";
            ((System.ComponentModel.ISupportInitialize)(this.img_in)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_out)).EndInit();
            this.chanel_groupBox.ResumeLayout(false);
            this.chanel_groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.histogram.ResumeLayout(false);
            this.histogram.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox img_in;
        private System.Windows.Forms.PictureBox img_out;
        private System.Windows.Forms.Button open_btn;
        private System.Windows.Forms.Button invert_btn;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button color_btn;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.RadioButton RGB;
        private System.Windows.Forms.RadioButton HSV;
        private System.Windows.Forms.RadioButton R;
        private System.Windows.Forms.RadioButton G;
        private System.Windows.Forms.RadioButton B;
        private System.Windows.Forms.RadioButton H;
        private System.Windows.Forms.RadioButton V;
        private System.Windows.Forms.RadioButton S;
        private System.Windows.Forms.GroupBox chanel_groupBox;
        private System.Windows.Forms.RadioButton M;
        private System.Windows.Forms.RadioButton C;
        private System.Windows.Forms.RadioButton Y;
        private System.Windows.Forms.RadioButton CMYK;
        private System.Windows.Forms.RadioButton uV;
        private System.Windows.Forms.RadioButton U;
        private System.Windows.Forms.RadioButton Yu;
        private System.Windows.Forms.RadioButton YUV;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox histogram;
        private System.Windows.Forms.RadioButton bCutoff;
        private System.Windows.Forms.RadioButton gCutoff;
        private System.Windows.Forms.RadioButton rCuttof;
        private System.Windows.Forms.Button noise1;
        private System.Windows.Forms.Button noise2;
        private System.Windows.Forms.Button noise3;
        private System.Windows.Forms.Button noise4;
        private System.Windows.Forms.Button noise5;
        private System.Windows.Forms.Button noise6;
        private System.Windows.Forms.RadioButton I;
        private System.Windows.Forms.Button Segment;
        private System.Windows.Forms.Button Roberts;
        private System.Windows.Forms.Button Prewitt;
        private System.Windows.Forms.Button treshold;
    }
}

