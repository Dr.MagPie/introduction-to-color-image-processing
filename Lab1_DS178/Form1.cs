﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Lab1_DS178
{
    public partial class Lab1_DmitrijsSorokins178 : Form
    {
        public Lab1_DmitrijsSorokins178()
        {
            InitializeComponent();
            img_in.SizeMode = PictureBoxSizeMode.Zoom;
            img_out.SizeMode = PictureBoxSizeMode.Zoom;
            label1.Visible = false;
            btn_invert.Visible = false;
            btn_save.Visible = false;
            btn_invert.Enabled = false;
            btn_save.Enabled = false;
            progressBar1.Visible = false;
            progressBar1.ForeColor = Color.Red;
            progressBar1.Style = ProgressBarStyle.Continuous;
        }

        private void Btn_open_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                img_in.Image = Bitmap.FromFile(openFileDialog1.FileName);
                btn_invert.Enabled = true;
                btn_invert.Visible = true;
            }
        }

        private void Btn_invert_Click(object sender, EventArgs e)
        {
            if(img_in.Image != null)
            {
                label1.Visible = true;
                img_out.Image = Invert((Bitmap)img_in.Image.Clone());
                btn_save.Enabled = true;
                btn_save.Visible = true;
            }
        }
        private void Btn_save_Click(object sender, EventArgs e)
        {
            if (img_out.Image != null)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    img_out.Image.Save(dialog.FileName, ImageFormat.Bmp);
                }
            }
        }

        Point p;
        Color boxColor = Color.Red;
        private void Img_in_MouseDown(object sender, MouseEventArgs e)
        {
            if (img_in.Image != null)
            {
                if (e.Button.ToString() == "Right")
                {
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {
                        label2.Text = colorDialog1.Color.ToString();
                        boxColor = colorDialog1.Color;
                    }
                }
                else
                {
                    p = ConvertXY(e.X, e.Y, img_in);
                    label2.Text = "X: " + Convert.ToString(p.X) + "\nY: " + Convert.ToString(p.Y);
                }                
            }
        }

        private void Img_in_MouseUp(object sender, MouseEventArgs e)
        {
            if(img_in.Image != null && p != null)
            {
                Point p2 = ConvertXY(e.X, e.Y, img_in);
                label2.Text = label2.Text + "\nX2: " + Convert.ToString(p2.X) + "\nY2: " + Convert.ToString(p2.Y);
                img_in.Image = DrawRect((Bitmap)img_in.Image, p, p2, boxColor);
            }
        }

        private Bitmap DrawRect(Bitmap image, Point p1, Point p2, Color color)
        {
            int x_small, x_big, y_small, y_big;
            if (p1.X < p2.X)
            {
                x_small = p1.X;
                x_big = p2.X;
            }
            else
            {
                x_small = p2.X;
                x_big = p1.X;
            }

            if (p1.Y < p2.Y)
            {
                y_small = p1.Y;
                y_big = p2.Y;
            }
            else
            {
                y_small = p2.Y;
                y_big = p1.Y;
            }


            for (int i = x_small; i < x_big; i++)
            {
                for (int j = y_small; j < y_big; j++)
                {
                    image.SetPixel(i, j, color);
                }
            }
            return image;
        }

        private Bitmap Invert(Bitmap image)
        {
            int r, g, b;
            int temp = image.Width / 100;
            int temp2 = temp;
            progressBar1.Value = 0;
            progressBar1.Visible = true;
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    r = 255 - image.GetPixel(i, j).R;
                    g = 255 - image.GetPixel(i, j).G;
                    b = 255 - image.GetPixel(i, j).B;
                    image.SetPixel(i, j, Color.FromArgb(r, g, b));
                }
                if(i > temp && progressBar1.Value < 100)
                {
                    progressBar1.Value++;
                    temp += temp2;
                }
            }
            progressBar1.Value = 100;
            progressBar1.Visible = false;
            return image;
        }

        private Point ConvertXY(int x, int y, PictureBox pictureBox)
        {
            Point p = new Point();

            int imgWidth = pictureBox.Image.Width;
            int imgHeight = pictureBox.Image.Height;
            int boxWidth = pictureBox.Width;
            int boxHeight = pictureBox.Height;

            double kx = (double)imgWidth / boxWidth;
            double ky = (double)imgHeight / boxHeight;
            double k = Math.Max(kx, ky);

            double offsetX = (boxWidth * k - imgWidth) / 2f;
            double offsetY = (boxHeight * k - imgHeight) / 2f;

            p.X = Convert.ToInt32(Math.Max(Math.Min(Math.Round(x * k - offsetX), imgWidth - 1), 0));
            p.Y = Convert.ToInt32(Math.Max(Math.Min(Math.Round(y * k - offsetY), imgHeight - 1), 0));

            return p;
        }
    }
}
