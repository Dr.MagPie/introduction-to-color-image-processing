﻿namespace Lab1_DS178
{
    partial class Lab1_DmitrijsSorokins178
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.img_in = new System.Windows.Forms.PictureBox();
            this.img_out = new System.Windows.Forms.PictureBox();
            this.btn_open = new System.Windows.Forms.Button();
            this.btn_invert = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.progressBar1 = new System.Windows.Forms.CustomProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.img_in)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_out)).BeginInit();
            this.SuspendLayout();
            // 
            // img_in
            // 
            this.img_in.Location = new System.Drawing.Point(12, 12);
            this.img_in.Name = "img_in";
            this.img_in.Size = new System.Drawing.Size(500, 500);
            this.img_in.TabIndex = 0;
            this.img_in.TabStop = false;
            this.img_in.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Img_in_MouseDown);
            this.img_in.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Img_in_MouseUp);
            // 
            // img_out
            // 
            this.img_out.Location = new System.Drawing.Point(599, 12);
            this.img_out.Name = "img_out";
            this.img_out.Size = new System.Drawing.Size(500, 500);
            this.img_out.TabIndex = 1;
            this.img_out.TabStop = false;
            // 
            // btn_open
            // 
            this.btn_open.BackColor = System.Drawing.SystemColors.Desktop;
            this.btn_open.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_open.Location = new System.Drawing.Point(518, 12);
            this.btn_open.Name = "btn_open";
            this.btn_open.Size = new System.Drawing.Size(75, 63);
            this.btn_open.TabIndex = 2;
            this.btn_open.Text = "Open";
            this.btn_open.UseVisualStyleBackColor = false;
            this.btn_open.Click += new System.EventHandler(this.Btn_open_Click);
            // 
            // btn_invert
            // 
            this.btn_invert.BackColor = System.Drawing.SystemColors.Desktop;
            this.btn_invert.Enabled = false;
            this.btn_invert.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_invert.Location = new System.Drawing.Point(518, 81);
            this.btn_invert.Name = "btn_invert";
            this.btn_invert.Size = new System.Drawing.Size(75, 63);
            this.btn_invert.TabIndex = 3;
            this.btn_invert.Text = "Invert";
            this.btn_invert.UseVisualStyleBackColor = false;
            this.btn_invert.Visible = false;
            this.btn_invert.Click += new System.EventHandler(this.Btn_invert_Click);
            // 
            // btn_save
            // 
            this.btn_save.BackColor = System.Drawing.SystemColors.Desktop;
            this.btn_save.Enabled = false;
            this.btn_save.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_save.Location = new System.Drawing.Point(518, 150);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 63);
            this.btn_save.TabIndex = 4;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Visible = false;
            this.btn_save.Click += new System.EventHandler(this.Btn_save_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 70F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(488, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 107);
            this.label1.TabIndex = 5;
            this.label1.Text = "→";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.Location = new System.Drawing.Point(518, 323);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 6;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.SystemColors.Desktop;
            this.progressBar1.Location = new System.Drawing.Point(-1, -4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1113, 536);
            this.progressBar1.TabIndex = 7;
            // 
            // Lab1_DmitrijsSorokins178
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1107, 523);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_invert);
            this.Controls.Add(this.btn_open);
            this.Controls.Add(this.img_in);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.img_out);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.Window;
            this.Name = "Lab1_DmitrijsSorokins178";
            this.Text = "Lab1_DmitrijsSorokins178";
            ((System.ComponentModel.ISupportInitialize)(this.img_in)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_out)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox img_in;
        private System.Windows.Forms.PictureBox img_out;
        private System.Windows.Forms.Button btn_open;
        private System.Windows.Forms.Button btn_invert;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CustomProgressBar progressBar1;
        private System.Windows.Forms.ColorDialog colorDialog1;
    }
}

